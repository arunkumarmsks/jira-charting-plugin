package com.atlassian.jira.ext.charting.gadgets.charts;

public interface ChartParamKeys
{
    String KEY_COMPLETE_DATASET = "completeDataset";

    String KEY_COMPLETE_DATASET_URL_GENERATOR = "completeDatasetUrlGenerator";
}
