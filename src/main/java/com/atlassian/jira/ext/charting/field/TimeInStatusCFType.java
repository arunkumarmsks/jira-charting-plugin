package com.atlassian.jira.ext.charting.field;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.customfields.converters.DatePickerConverter;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.util.EasyList;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.DelegatorInterface;
import org.ofbiz.core.entity.GenericEntityException;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This custom field will calculate the amount of time spent in a specific status.
 */
public class TimeInStatusCFType extends CalculatedCFType
{

    private static final Logger log = Logger.getLogger(TimeInStatusCFType.class);
    protected static final PersistenceFieldType PERSISTENCE_TYPE = PersistenceFieldType.TYPE_UNLIMITED_TEXT;
    protected static final String NULL_STRING = "null";   // a database marker to indicate that we could not determine the fields value
    private DelegatorInterface delegatorInterface;
    protected final DatePickerConverter dateConverter;
    protected final CustomFieldValuePersister customFieldValuePersister;
    private TimeInStatusDAO timeInStatusDAO;
    private static final String CUSTOMFIELD = "customfield";
    private static final String ISSUE = "issue";
    private static final String CUSTOM_FIELD_VALUE_ENTITY_NAME = "CustomFieldValue";

    public TimeInStatusCFType(DatePickerConverter dateConverter, CustomFieldValuePersister customFieldValuePersister)
    {
        this.delegatorInterface = getDelegatorInterface();
        this.dateConverter = dateConverter;
        this.customFieldValuePersister = customFieldValuePersister;
        this.timeInStatusDAO = getTimeInStatusDAO(delegatorInterface);
    }

    protected DelegatorInterface getDelegatorInterface()
    {
        return ComponentAccessor.getComponent(DelegatorInterface.class);
    }

    protected TimeInStatusDAO getTimeInStatusDAO(DelegatorInterface delegatorInterface) {
        return new TimeInStatusDAO(delegatorInterface);
    }

    protected String getValueForStatuses(Issue issue)
    {
        return timeInStatusDAO.calculateForStatuses(issue);
    }

    public Object getValueFromIssue(CustomField field, Issue issue)
    {
        String dbString = retrieveDatabaseValue(field, issue);
        String stringValue = dbString;

        // if issue is resolved and db value is null, then calculate date and store
        if (issue.getResolution() != null && dbString == null)
        {
            stringValue = getValueForStatuses(issue);

            // this should never happen - only if an issue is resolved, but without a change history.
            if (stringValue == null)
            {
                stringValue = NULL_STRING;
            }

            storeDatabaseValue(field, issue, stringValue);
        }
        // if issue is NOT resolved and db value is NOT null, then remove
        else if (issue.getResolution() == null && dbString != null)
        {
            clearDatabaseValue(field, issue, dbString);
            return null;
        }

        if (NULL_STRING.equals(stringValue))
        {
            return null;
        }
        else
        {
            return stringValue;
        }
    }

    protected void storeDatabaseValue(CustomField field, Issue issue, String fieldValue)
    {
        customFieldValuePersister.updateValues(field, issue.getId(), PERSISTENCE_TYPE, EasyList.build(fieldValue));
    }

    protected void clearDatabaseValue(CustomField field, Issue issue, String dbString)
    {
        // NOTE: This is a hack. We were calling the customFieldValuePersister.removeValue method call, but it does
        // not properly handle a persistent type of unlimited text. The method does a string compare and in mssql
        // the equals compare of an unbounded text field blows up. We should use the removeValue method when it is
        // fixed up see JRA-11170
        try
        {
            final Map limitClause = EasyMap.build(CUSTOMFIELD, CustomFieldUtils.getCustomFieldId(field.getId()), ISSUE, issue.getId());
            delegatorInterface.removeByAnd(CUSTOM_FIELD_VALUE_ENTITY_NAME, limitClause);
        }
        catch (GenericEntityException e)
        {
            log.warn("Unable to remove value for TimeInStatusCFT for issue: " + issue.getId());
        }
    }

    protected String retrieveDatabaseValue(CustomField field, Issue issue)
    {
        String value;
        final List values = customFieldValuePersister.getValues(field, issue.getId(), PERSISTENCE_TYPE);
        if (values.isEmpty())
        {
            value = null;
        }
        else if (values.size() > 1)
        {
            log.error("More than one value stored for custom field id'" + field.getId() + "'.  Values " + values);
            value = null;
        }
        else
        {
            value = (String) values.get(0);
        }

        return value;
    }

    public Set remove(CustomField field)
    {
        return customFieldValuePersister.removeAllValues(field.getId());
    }

    public String getStringFromSingularObject(Object singularObject)
    {
        assertObjectImplementsType(String.class, singularObject);
        return singularObject.toString();
    }

    public Object getSingularObjectFromString(String string) throws FieldValidationException
    {
        if (string == null)
        {
            return null;
        }
        return string;
    }

}
