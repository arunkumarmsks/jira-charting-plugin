package com.atlassian.jira.ext.charting.gadgets.charts;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.charts.Chart;
import com.atlassian.jira.charts.ChartFactory;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;

import java.io.IOException;
import java.util.Set;

public class AverageNumberOfTimesInStatusChart extends AverageStatusChart
{
    public AverageNumberOfTimesInStatusChart(CustomFieldManager customFieldManager, ConstantsManager constantsManager,
            IssueIndexManager issueIndexManager, SearchProvider searchProvider, SearchService searchService,
            ApplicationProperties applicationProperties, TimeZoneManager timeZoneManager,
            VelocityRequestContextFactory velocityRequestContextFactory)
    {
        super(customFieldManager, constantsManager, issueIndexManager, searchProvider, searchService,
                applicationProperties, timeZoneManager, velocityRequestContextFactory);
    }

    public Chart generate(final JiraAuthenticationContext jiraAuthenticationContext,
                          final SearchRequest searchRequest,
                          final Set<String> statusIds,
                          final ChartFactory.PeriodName periodName,
                          final int daysPrevious,
                          final int width,
                          final int height) throws SearchException, IOException
    {
        I18nHelper i18nHelper = jiraAuthenticationContext.getI18nHelper();

        return generate(jiraAuthenticationContext, searchRequest, statusIds, periodName, daysPrevious, null, width, height,
                i18nHelper.getText("datacollector.numberinstatus"),
                i18nHelper.getText("datacollector.tooltip.times"));
    }

    public Chart generateInline(final JiraAuthenticationContext jiraAuthenticationContext,
            final SearchRequest searchRequest, final Set<String> statusIds, final ChartFactory.PeriodName periodName,
            final int daysPrevious, final int width, final int height) throws SearchException, IOException
    {
        I18nHelper i18nHelper = jiraAuthenticationContext.getI18nHelper();

        return generateInline(jiraAuthenticationContext, searchRequest, statusIds, periodName, daysPrevious, null, width,
                height, i18nHelper.getText("datacollector.numberinstatus"),
                i18nHelper.getText("datacollector.tooltip.times"));
    }

}
