package com.atlassian.jira.ext.charting.field;

import com.atlassian.jira.imports.project.customfield.NoTransformationCustomFieldImporter;
import com.atlassian.jira.imports.project.customfield.ProjectCustomFieldImporter;
import com.atlassian.jira.imports.project.customfield.ProjectImportableCustomField;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.DatePickerConverter;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.util.EasyList;
import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Set;

public abstract class AbstractCalculatedDateCFType extends CalculatedCFType implements ProjectImportableCustomField
{
    private static final Logger log = Logger.getLogger(AbstractCalculatedDateCFType.class);
    protected static final PersistenceFieldType PERSISTENCE_TYPE = PersistenceFieldType.TYPE_DATE;
    protected static final Timestamp NULL_TIMESTAMP = new Timestamp(0);   // a database marker to indicate that we could not determine the resolution date
    protected final DatePickerConverter dateConverter;
    protected final CustomFieldValuePersister customFieldValuePersister;

    public AbstractCalculatedDateCFType(DatePickerConverter dateConverter, CustomFieldValuePersister customFieldValuePersister)
    {
        this.dateConverter = dateConverter;
        this.customFieldValuePersister = customFieldValuePersister;
    }

    public abstract Object getValueFromIssue(CustomField field, Issue issue);

    protected void storeDatabaseValue(CustomField field, Issue issue, Date date)
    {
        customFieldValuePersister.updateValues(field, issue.getId(), PERSISTENCE_TYPE, EasyList.build(date));
    }

    protected void clearDatabaseValue(CustomField field, Issue issue, Date dbDate)
    {
        customFieldValuePersister.removeValue(field, issue.getId(), PERSISTENCE_TYPE, dbDate);
    }

    protected Timestamp retrieveDatabaseValue(CustomField field, Issue issue)
    {
        Timestamp value;
        final List values = customFieldValuePersister.getValues(field, issue.getId(), PERSISTENCE_TYPE);
        if (values.isEmpty())
        {
            value = null;
        }
        else if (values.size() > 1)
        {
            log.error("More than one value stored for custom field id'" + field.getId() + "'.  Values " + values);
            value = null;
        }
        else
        {
            value = (Timestamp) values.get(0);
        }

        return value;
    }

    public Set remove(CustomField field)
    {
        return customFieldValuePersister.removeAllValues(field.getId());
    }

    public String getStringFromSingularObject(Object singularObject)
    {
        assertObjectImplementsType(Date.class, singularObject);
        return dateConverter.getString((Date) singularObject);
    }

    public Object getSingularObjectFromString(String string) throws FieldValidationException
    {
        return dateConverter.getTimestamp(string);
    }
    
    public ProjectCustomFieldImporter getProjectImporter()
    {
        return new NoTransformationCustomFieldImporter();
    }
}
