package com.atlassian.jira.ext.charting.configurableobjects;

import com.atlassian.configurable.ValuesGenerator;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.status.Status;
import org.apache.commons.collections.map.ListOrderedMap;

import java.util.Map;

/**
 * Will generate all the statuses in the system.
 */
public class AllStatusesValuesGenerator implements ValuesGenerator
{

    public Map getValues(Map userParams)
    {
        ConstantsManager constantsManager = getConstantsManager();
        @SuppressWarnings("unchecked")
        Map<String, String> values = new ListOrderedMap();
        for (Status status : constantsManager.getStatusObjects())
        {
            values.put(status.getId(), status.getName());
        }
        return values;
    }

// Setter/getter need not to be tested
///CLOVER:OFF
    protected ConstantsManager getConstantsManager()
    {
        return ComponentAccessor.getConstantsManager();
    }
///CLOVER:ON
}
