package com.atlassian.jira.ext.charting.data;

import com.atlassian.jira.issue.statistics.util.DocumentHitCollector;
import com.atlassian.jira.util.LuceneUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;
import org.jfree.data.time.RegularTimePeriod;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class DateRangeObjectHitCollector extends DocumentHitCollector
{
    private final String dateDocumentConstant1;

    private final String dateDocumentConstant2;

    private final Map<RegularTimePeriod, List<Long>> result;

    private final Class timePeriodClass;

    private final TimeZone userTimeZone;

    public DateRangeObjectHitCollector(String dateDocumentConstant1, String dateDocumentConstant2, Map<RegularTimePeriod, List<Long>> result, IndexSearcher searcher, Class timePeriodClass, TimeZone userTimeZone)
    {
        super(searcher);
        this.dateDocumentConstant1 = dateDocumentConstant1;
        this.dateDocumentConstant2 = dateDocumentConstant2;
        this.result = result;
        this.timePeriodClass = timePeriodClass;
        this.userTimeZone = userTimeZone;
    }

    public void collect(Document d)
    {
        Date creationDate = LuceneUtils.stringToDate(d.get(dateDocumentConstant1));
        Date otherDate = LuceneUtils.stringToDate(d.get(dateDocumentConstant2));

        RegularTimePeriod period = RegularTimePeriod.createInstance(timePeriodClass, otherDate, userTimeZone);

        List<Long> values = result.get(period);
        if (values == null)
            values = new ArrayList<Long>();

        values.add(otherDate.getTime() - creationDate.getTime());
        result.put(period, values);
    }
}
