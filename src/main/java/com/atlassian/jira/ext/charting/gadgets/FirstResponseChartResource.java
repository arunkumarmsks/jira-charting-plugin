package com.atlassian.jira.ext.charting.gadgets;

import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.charts.Chart;
import com.atlassian.jira.charts.ChartFactory;
import com.atlassian.jira.charts.jfreechart.TimePeriodUtils;
import com.atlassian.jira.charts.util.ChartUtils;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.ext.charting.gadgets.charts.TimeToFirstResponseChart;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.rest.v1.model.errors.ErrorCollection;
import com.atlassian.jira.rest.v1.model.errors.ValidationError;
import com.atlassian.jira.rest.v1.util.CacheControl;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.util.OutlookDateManager;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import org.apache.commons.httpclient.HttpStatus;
import org.jfree.chart.urls.CategoryURLGenerator;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.time.RegularTimePeriod;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import static com.atlassian.jira.ext.charting.gadgets.charts.ChartParamKeys.KEY_COMPLETE_DATASET;
import static com.atlassian.jira.ext.charting.gadgets.charts.ChartParamKeys.KEY_COMPLETE_DATASET_URL_GENERATOR;

@Path("firstresponse")
@AnonymousAllowed
public class FirstResponseChartResource extends ProjectOrFilterIdBasedChartResource
{
    private final CustomFieldManager customFieldManager;
    private final IssueIndexManager issueIndexManager;
    private final SearchProvider searchProvider;
    private final TimeZoneManager timeZoneManager;

    public FirstResponseChartResource(JiraAuthenticationContext jiraAuthenticationContext, ProjectManager projectManager,
            SearchRequestService searchRequestService, SearchService searchService, ChartUtils chartUtils,
            PermissionManager permissionManager, VelocityRequestContextFactory velocityRequestContextFactory,
            ApplicationProperties applicationProperties, CustomFieldManager customFieldManager,
            IssueIndexManager issueIndexManager, SearchProvider searchProvider, OutlookDateManager outlookDateManager,
            TimeZoneManager timeZoneManager)
    {
        super(jiraAuthenticationContext, projectManager, searchRequestService, searchService, chartUtils, permissionManager, velocityRequestContextFactory, applicationProperties);
        this.customFieldManager = customFieldManager;
        this.issueIndexManager = issueIndexManager;
        this.searchProvider = searchProvider;
        this.timeZoneManager = timeZoneManager;
    }

    @GET
    @Path("generate")
    @Produces({MediaType.APPLICATION_JSON})
    public Response generate(
            @QueryParam("projectOrFilterId") String projectOrFilterId,
            @QueryParam("periodName") @DefaultValue("daily") String periodName,
            @QueryParam("daysprevious") @DefaultValue("30") int daysPrevious,
            @QueryParam("width") @DefaultValue("400") int width,
            @QueryParam("height") @DefaultValue("250") int height,
            @QueryParam("returnData") @DefaultValue("false") boolean returnData,
            @QueryParam (INLINE) @DefaultValue ("false") final boolean inline) throws SearchException, IOException
    {
        Map<String, Object> params = new HashMap<String, Object>();

        SearchRequest searchRequest = new SearchRequest(chartUtils.retrieveOrMakeSearchRequest(projectOrFilterId, params));
        TimeToFirstResponseChart chart = new TimeToFirstResponseChart(
                customFieldManager, issueIndexManager, searchProvider, searchService,
                applicationProperties, timeZoneManager, velocityRequestContextFactory);

        try
        {
            Chart theChart = inline ? chart.generateInline(
                    jiraAuthenticationContext,
                    searchRequest,
                    ChartFactory.PeriodName.valueOf(periodName),
                    daysPrevious,
                    width,
                    height
            ) : chart.generate(
                    jiraAuthenticationContext,
                    searchRequest,
                    ChartFactory.PeriodName.valueOf(periodName),
                    daysPrevious,
                    width,
                    height
            );

            params.putAll(theChart.getParameters());

            FirstResponseChartJson firstResponseChartJson = new FirstResponseChartJson(
                    theChart.getLocation(),
                    theChart.getImageMap(),
                    theChart.getImageMapName(),
                    width,
                    height,
                    getProjectNameOrFilterTitle(projectOrFilterId),
                    getFilterUrl(params),
                    daysPrevious,
                    theChart.getBase64Image());

            if (returnData)
                firstResponseChartJson.data = getData(theChart.getParameters());

            return Response.ok(firstResponseChartJson).cacheControl(CacheControl.NO_CACHE).build();
        }
        catch (IllegalStateException ise)
        {
            return createServiceUnavailableErrorMessageResponse(jiraAuthenticationContext.getI18nHelper().getText("portlet.firstresponsetime.noissues"));
        }
    }

    @XmlRootElement
    public static class FirstResponseDataRow
    {
        @XmlElement
        private String period;

        @XmlElement
        private long hours;

        @XmlElement
        private String url;

        public FirstResponseDataRow()
        {
        }

        public FirstResponseDataRow(String period, long hours, String url)
        {
            this.period = period;
            this.hours = hours;
            this.url = url;
        }
    }

    List<FirstResponseDataRow> getData(Map<String, Object> chartParams)
    {
        CategoryURLGenerator completeUrlGenerator = (CategoryURLGenerator) chartParams.get(KEY_COMPLETE_DATASET_URL_GENERATOR);
        CategoryDataset completeDataset = (CategoryDataset) chartParams.get(KEY_COMPLETE_DATASET);
        return generateDataSet(completeDataset, completeUrlGenerator);
    }

    List<FirstResponseDataRow> generateDataSet(CategoryDataset dataset, CategoryURLGenerator urlGenerator)
    {
        TimePeriodUtils timePeriodUtils = new TimePeriodUtils(timeZoneManager);
        List<FirstResponseDataRow> firstResponseDataRows = new ArrayList<FirstResponseDataRow>();

        for (int col = 0; col < dataset.getColumnCount(); col++)
        {
            RegularTimePeriod regularTimePeriod = (RegularTimePeriod) dataset.getColumnKey(col);

            String period = timePeriodUtils.prettyPrint(regularTimePeriod);
            int val = dataset.getValue(2, col).intValue();
            String url = urlGenerator.generateURL(dataset, 0, col);

            firstResponseDataRows.add(new FirstResponseDataRow(period, val, url));
        }

        return firstResponseDataRows;
    }

    @XmlRootElement
    public static class FirstResponseChartJson
    {
        @XmlElement
        private String location;

        @XmlElement
        private String imageMap;

        @XmlElement
        private String imageMapName;

        @XmlElement
        private int width;

        @XmlElement
        private int height;

        @XmlElement
        private String filterTitle;

        @XmlElement
        private String filterUrl;

        @XmlElement
        private int daysprevious;

        @XmlElement
        private List<FirstResponseDataRow> data;

        @XmlElement
        private String base64Image;

        public FirstResponseChartJson(String location, String imageMap, String imageMapName, int width, int height, String filterTitle, String filterUrl, int daysprevious, String base64Image)
        {
            this.location = location;
            this.imageMap = imageMap;
            this.imageMapName = imageMapName;
            this.width = width;
            this.height = height;
            this.filterTitle = filterTitle;
            this.filterUrl = filterUrl;
            this.daysprevious = daysprevious;
            this.base64Image = base64Image;
        }

        public FirstResponseChartJson()
        {
        }
    }


    @GET
    @Path("config/validate")
    @Produces({MediaType.APPLICATION_JSON})
    public Response validate(
            @QueryParam("projectOrFilterId") String projectOrFilterId,
            @QueryParam("daysprevious") String daysPrevious)
    {
        Collection<ValidationError> validationErrors = new ArrayList<ValidationError>();
        validateProjectOrFilterId(projectOrFilterId, validationErrors);
        validateDaysPrevious(daysPrevious, validationErrors);

        if (validationErrors.isEmpty())
        {
            return Response.ok().cacheControl(CacheControl.NO_CACHE).build();
        }
        else
        {
            return Response.status(HttpStatus.SC_BAD_REQUEST).entity(ErrorCollection.Builder.newBuilder(validationErrors).build()).cacheControl(CacheControl.NO_CACHE).build();
        }
    }
}
