package com.atlassian.jira.ext.charting.field;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.DatePickerConverter;
import com.atlassian.jira.issue.customfields.converters.DatePickerConverterImpl;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.DateFieldFormat;
import com.atlassian.jira.util.I18nHelper;
import junit.framework.TestCase;
import org.apache.commons.lang.time.FastDateFormat;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.anyList;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestSimpleCalculatedDateCFType extends TestCase
{
    private AbstractCalculatedDateCFType abstractCalculatedDateCFType;

    private DatePickerConverter datePickerConverter;
    
    @Mock private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock private CustomFieldValuePersister customFieldValuePersister;
    @Mock private Issue issue;
    @Mock private CustomField customField;
    @Mock private ApplicationProperties applicationProperties;
    @Mock private I18nHelper.BeanFactory beanFactory;
    @Mock private DateFieldFormat dateFieldFormat;

    protected void setUp() throws Exception {
        super.setUp();

        MockitoAnnotations.initMocks(this);
        
        datePickerConverter = new DatePickerConverterImpl(jiraAuthenticationContext, dateFieldFormat);
    }

    public void testStoreDatabaseValue() {
        final Long issueId = new Long(1);
        
        when(issue.getId()).thenReturn(issueId);

        abstractCalculatedDateCFType = new SimpleCalculatedDateCFType(datePickerConverter, customFieldValuePersister);
        abstractCalculatedDateCFType.storeDatabaseValue(customField, issue, new Date());

        verify(issue, atLeastOnce()).getId();
        verify(customFieldValuePersister).updateValues(eq(customField), anyLong(), eq(PersistenceFieldType.TYPE_DATE), anyList());
    }

    public void testClearDatabaseValue() {
        final Long issueId = new Long(1);

        when(customFieldValuePersister.removeValue(customField, issueId, PersistenceFieldType.TYPE_DATE, Object.class)).thenReturn(Collections.<Long>emptySet());
        when(issue.getId()).thenReturn(issueId);

        abstractCalculatedDateCFType = new SimpleCalculatedDateCFType(datePickerConverter, customFieldValuePersister);
        abstractCalculatedDateCFType.clearDatabaseValue(customField, issue, new Date());
        
        verify(issue, atLeastOnce()).getId();
        verify(customFieldValuePersister).removeValue(eq(customField), anyLong(), eq(PersistenceFieldType.TYPE_DATE), anyObject());
    }

    public void retrieveDatabaseValueWhenThereIsNoValue() {
        final Long issueId = new Long(1);

        /* When an no values are returned */
        when(customFieldValuePersister.getValues(customField, issueId, PersistenceFieldType.TYPE_DATE)).thenReturn(Collections.emptyList());
        verify(issue, atLeastOnce()).getId();

        abstractCalculatedDateCFType = new SimpleCalculatedDateCFType(datePickerConverter, customFieldValuePersister);
        
        assertNull(abstractCalculatedDateCFType.retrieveDatabaseValue(customField, issue));
        
        verify(issue, atLeastOnce()).getId();
        verify(customFieldValuePersister).getValues(customField, issueId, PersistenceFieldType.TYPE_DATE);
    }


    public void retrieveDatabaseValueWhenThereAreMoreThanOneValues() {
        final Long issueId = new Long(1);

        when(customFieldValuePersister.getValues(customField, issueId, PersistenceFieldType.TYPE_DATE)).thenReturn(
                new ArrayList<Object>(Arrays.asList(new Timestamp(System.currentTimeMillis()), new Timestamp(System.currentTimeMillis())))
        );

        abstractCalculatedDateCFType = new SimpleCalculatedDateCFType(datePickerConverter, customFieldValuePersister);

        assertNull(abstractCalculatedDateCFType.retrieveDatabaseValue(customField, issue));
        
        verify(issue, atLeastOnce()).getId();
        verify(customFieldValuePersister).getValues(customField, issueId, PersistenceFieldType.TYPE_DATE);
    }

    public void retrieveDatabaseValueWhenThereIsOnlyOneValue() {
        final Long issueId = new Long(1);
        final Timestamp returnValue = new Timestamp(Long.MAX_VALUE);

        when(customFieldValuePersister.getValues(customField, issueId, PersistenceFieldType.TYPE_DATE)).thenReturn(
                new ArrayList<Object>(Arrays.asList(returnValue))
        );

        abstractCalculatedDateCFType = new SimpleCalculatedDateCFType(datePickerConverter, customFieldValuePersister);

        assertSame(returnValue, abstractCalculatedDateCFType.retrieveDatabaseValue(customField, issue));
        
        verify(issue, atLeastOnce()).getId();
        verify(customFieldValuePersister).getValues(customField, issueId, PersistenceFieldType.TYPE_DATE);
    }

    public void testRemove() {
        when(customField.getId()).thenReturn("customfield_10000");
        when(customFieldValuePersister.removeAllValues(customField.getId())).thenReturn(Collections.EMPTY_SET);
        
        abstractCalculatedDateCFType = new SimpleCalculatedDateCFType(datePickerConverter, customFieldValuePersister);

        assertSame(Collections.emptySet(), abstractCalculatedDateCFType.remove(customField));
        verify(customFieldValuePersister, times(2)).removeAllValues(customField.getId());
    }

    public void testGetStringFromSingularObject() {
        final Date now = new Date();
        final String expectedString = FastDateFormat.getInstance("dd-MM-yyyy").format(now);

        when(dateFieldFormat.formatDatePicker(Matchers.<Date>any())).thenReturn(expectedString);

        abstractCalculatedDateCFType = new SimpleCalculatedDateCFType(datePickerConverter, customFieldValuePersister);

        assertEquals(expectedString, abstractCalculatedDateCFType.getStringFromSingularObject(now));
    }

    public void testGetSingularObjectFromString() {
        final Date expectedDate = new Date();

        final String inputDateString = FastDateFormat.getInstance("dd-MM-yyyy").format(expectedDate);

        when(dateFieldFormat.parseDatePicker(anyString())).thenReturn(expectedDate);

        abstractCalculatedDateCFType = new SimpleCalculatedDateCFType(datePickerConverter, customFieldValuePersister);
        
        assertEquals(expectedDate, abstractCalculatedDateCFType.getSingularObjectFromString(inputDateString));
    }

    private static class SimpleCalculatedDateCFType extends AbstractCalculatedDateCFType {

        public SimpleCalculatedDateCFType(final DatePickerConverter datePickerConverter, final CustomFieldValuePersister customFieldValuePersister) {
            super(datePickerConverter, customFieldValuePersister);
        }

        public Object getValueFromIssue(CustomField field, Issue issue) {
            return null;
        }
    }
}
