package com.atlassian.jira.ext.charting.gadgets;

import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.charts.util.ChartUtils;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.ext.charting.gadgets.AverageStatusChartResource.AverageStatusDataRow;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.util.OutlookDate;
import com.atlassian.jira.web.util.OutlookDateManager;
import junit.framework.TestCase;
import org.jfree.chart.urls.XYURLGenerator;
import org.jfree.data.time.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static org.mockito.Mockito.when;

public class AverageStatusChartResourceTestCase extends TestCase
{
    private AverageStatusChartResource averageStatusChartResource;
    
    private Locale locale;
    private List<AverageStatusDataRow> testData;
    private int count;
    
    @Mock private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock private ProjectManager projectManager;
    @Mock private SearchRequestService searchRequestService;
    @Mock private SearchService searchService;
    @Mock private ChartUtils chartUtils;
    @Mock private PermissionManager permissionManager;
    @Mock private VelocityRequestContextFactory velocityRequestContextFactory;
    @Mock private ApplicationProperties applicationProperties;
    @Mock private CustomFieldManager customFieldManager;
    @Mock private ConstantsManager constantsManager;
    @Mock private IssueIndexManager issueIndexManager;
    @Mock private SearchProvider searchProvider;
    @Mock private OutlookDateManager outlookDateManager;
    @Mock private TimeSeriesCollection dataset;
    @Mock private XYURLGenerator urlGenerator;
    @Mock private OutlookDate outlookDate;
    @Mock private Comparable status;
    @Mock TimeZoneManager timeZoneManager;
    @Mock TimeSeries series;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);
        
        locale = Locale.US;
        
        averageStatusChartResource = new AverageStatusChartResource(jiraAuthenticationContext, 
                projectManager, searchRequestService, searchService, chartUtils, permissionManager, 
                velocityRequestContextFactory, applicationProperties, customFieldManager, 
                constantsManager, issueIndexManager, searchProvider, timeZoneManager);
        
        when(jiraAuthenticationContext.getLocale()).thenReturn(locale);
        when(outlookDateManager.getOutlookDate(locale)).thenReturn(outlookDate);
    }

    public void testGenerateDataSetWithoutAnyData()
    {
        count = 0;
        
        when(dataset.getSeriesCount()).thenReturn(count);
        
        testData = averageStatusChartResource.generateDataSet(dataset, urlGenerator);
        
        assertNotNull(testData);
    }
    
    public void testGenerateDataSetWithDataEntry()
    {
        count = 1;
        int seriesIndex = 0;
        int xyItemIndex = 0;
        Number periodInMillis = new Integer(100);
        Number value = new Double(10.01);
        TimeSeriesDataItem dataItem = new TimeSeriesDataItem(RegularTimePeriod.createInstance(Day.class, new Date(periodInMillis.longValue()), TimeZone.getDefault()), value);

        when(dataset.getSeriesCount()).thenReturn(count);
        when(dataset.getSeriesKey(seriesIndex)).thenReturn(status);
        when(dataset.getItemCount(seriesIndex)).thenReturn(count);
        when(dataset.getSeries(seriesIndex)).thenReturn(series);
        when(series.getDataItem(xyItemIndex)).thenReturn(dataItem);
        when(dataset.getX(seriesIndex, xyItemIndex)).thenReturn(periodInMillis);
        when(dataset.getY(seriesIndex, xyItemIndex)).thenReturn(value);
        
        testData = averageStatusChartResource.generateDataSet(dataset, urlGenerator);
        
        assertFalse(testData.isEmpty());
    }
}
