package com.atlassian.jira.ext.charting.field;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.issue.Issue;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.ofbiz.core.entity.GenericDelegator;
import org.ofbiz.core.entity.GenericEntityException;

import java.sql.Timestamp;
import java.util.Collections;

import static org.mockito.Mockito.when;

public class TestDateOfFirstResponseDAO extends TestCase
{
    GenericDelegator genericDelegator = GenericDelegator.getGenericDelegator("test");
    DateOfFirstResponseDAO dao = new DateOfFirstResponseDAO(genericDelegator);

    final Timestamp date10 = new Timestamp(10);
    final Timestamp date20 = new Timestamp(20);
    private static final String ISSUE_REPORTER = "test-reporter";
    private static final String ANOTHER_COMMENTER = "new-author";
    
    @Mock private Issue issue;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);
    }

    public TestDateOfFirstResponseDAO()
    {
        dao.ENTITY_DS = "testDS"; //this is because we are using a test datasource
    }

    protected void tearDown() throws Exception
    {
        genericDelegator.removeByAnd("Issue", Collections.EMPTY_MAP);
        genericDelegator.removeByAnd("Action", Collections.EMPTY_MAP);
    }

    public void testSelectNullWithNoActions() throws GenericEntityException
    {
        createIssue();
        assertCorrectDateReturned(null);
    }

    public void testSelectNullOnlyReporterActionsAndMultipleIssues() throws GenericEntityException
    {
        createIssue();
        genericDelegator.create("Issue", EasyMap.build("id", new Long(2), "key", "ABC-124", "reporter", "another-reporter")).store();
        genericDelegator.create("Action", EasyMap.build("id", new Long(10), "issue", new Long(1), "type", "comment", "author", ISSUE_REPORTER, "created", date10)).store();
        assertCorrectDateReturned(null);
    }

    public void testSelectReturnsCorrectDateWithOneComment() throws GenericEntityException
    {
        createIssue();
        genericDelegator.create("Action", EasyMap.build("id", new Long(10), "issue", new Long(1), "type", "comment", "author", ANOTHER_COMMENTER, "created", date10)).store();

        assertCorrectDateReturned(date10);
    }

    public void testSelectReturnsFirstDateWithTwoComments() throws GenericEntityException
    {
        createIssue();
        genericDelegator.create("Action", EasyMap.build("id", new Long(10), "issue", new Long(1), "type", "comment", "author", ANOTHER_COMMENTER, "created", date10)).store();
        genericDelegator.create("Action", EasyMap.build("id", new Long(11), "issue", new Long(1), "type", "comment", "author", ANOTHER_COMMENTER, "created", date20)).store();

        assertCorrectDateReturned(date10);
    }

    public void testSelectIgnoresActionsOtherThanComments() throws GenericEntityException
    {
        createIssue();
        genericDelegator.create("Action", EasyMap.build("id", new Long(10), "issue", new Long(1), "type", "worklog", "author", ANOTHER_COMMENTER, "created", date10)).store();
        genericDelegator.create("Action", EasyMap.build("id", new Long(11), "issue", new Long(1), "type", "comment", "author", ANOTHER_COMMENTER, "created", date20)).store();

        assertCorrectDateReturned(date20);
    }

    public void testSelectIgnoresCommentsMadeByReporter() throws GenericEntityException
    {
        createIssue();
        genericDelegator.create("Action", EasyMap.build("id", new Long(10), "issue", new Long(1), "type", "comment", "author", ISSUE_REPORTER, "created", date10)).store();
        genericDelegator.create("Action", EasyMap.build("id", new Long(11), "issue", new Long(1), "type", "comment", "author", ANOTHER_COMMENTER, "created", date20)).store();

        assertCorrectDateReturned(date20);
    }

    public void testComplicatedActionsWithManyCommentersAndActions() throws GenericEntityException
    {
        createIssue();
        genericDelegator.create("Action", EasyMap.build("id", new Long(10), "issue", new Long(1), "type", "worklog", "author", ANOTHER_COMMENTER, "created", date10)).store();
        genericDelegator.create("Action", EasyMap.build("id", new Long(11), "issue", new Long(1), "type", "comment", "author", ISSUE_REPORTER, "created", date20)).store();
        genericDelegator.create("Action", EasyMap.build("id", new Long(12), "issue", new Long(1), "type", "worklog", "author", "commenter-3", "created", new Timestamp(30))).store();
        genericDelegator.create("Action", EasyMap.build("id", new Long(13), "issue", new Long(1), "type", "fieldchange", "author", "fieldchange-author", "created", new Timestamp(40))).store();
        genericDelegator.create("Action", EasyMap.build("id", new Long(14), "issue", new Long(1), "type", "comment", "author", "commenter-4", "created", new Timestamp(50))).store();
        genericDelegator.create("Action", EasyMap.build("id", new Long(15), "issue", new Long(1), "type", "comment", "author", "commenter-5", "created", new Timestamp(60))).store();

        assertCorrectDateReturned(new Timestamp(50));
    }

    //----------------- Helper Methods ----------------------//

    private void assertCorrectDateReturned(Timestamp timestamp)
    {
        when(issue.getId()).thenReturn(1L);
        assertEquals(timestamp, dao.calculateDate(issue));
    }

    private void createIssue() throws GenericEntityException
    {
        genericDelegator.create("Issue", EasyMap.build("id", new Long(1), "key", "ABC-123", "reporter", ISSUE_REPORTER)).store();
    }
}