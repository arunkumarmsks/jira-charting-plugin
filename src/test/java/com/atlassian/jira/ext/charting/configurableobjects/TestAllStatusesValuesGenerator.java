package com.atlassian.jira.ext.charting.configurableobjects;

import com.atlassian.configurable.ValuesGenerator;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.status.Status;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.when;

public class TestAllStatusesValuesGenerator extends AbstractValuesGeneratorTestCase 
{
    private List<Status> statusList;
    
    @Mock private ConstantsManager constantsManager;
    @Mock private Status fooStatus1;
    @Mock private Status fooStatus2;

    protected void setUp() throws Exception 
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);
        
        statusList = new ArrayList<Status>();
        statusList.add(fooStatus1);
        statusList.add(fooStatus2);

        when(fooStatus1.getId()).thenReturn("fooStatus1Id");
        when(fooStatus1.getName()).thenReturn("fooStatus1");

        when(fooStatus2.getId()).thenReturn("fooStatus2Id");
        when(fooStatus2.getName()).thenReturn("fooStatus2");
        
        when(constantsManager.getStatusObjects()).thenReturn(statusList);
    }

    protected ValuesGenerator getValuesGenerator() {
        return new AllStatusesValuesGenerator() {
            protected ConstantsManager getConstantsManager() {
                return constantsManager;
            }
        };
    }

    public void testGetValues() {
        final Map values = getValuesGenerator().getValues(null);

        assertEquals(2, values.size());
        assertTrue(values.containsKey("fooStatus1Id"));
        assertEquals("fooStatus1", values.get("fooStatus1Id"));
        assertTrue(values.containsKey("fooStatus2Id"));
        assertEquals("fooStatus2", values.get("fooStatus2Id"));
    }
}
