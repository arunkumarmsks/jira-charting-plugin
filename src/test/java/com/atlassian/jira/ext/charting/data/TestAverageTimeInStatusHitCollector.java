package com.atlassian.jira.ext.charting.data;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.statistics.util.DocumentHitCollector;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.util.EasyList;
import com.atlassian.jira.util.LuceneUtils;
import junit.framework.TestCase;
import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.search.IndexSearcher;
import org.jfree.data.time.Day;
import org.jfree.data.time.RegularTimePeriod;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.ofbiz.core.entity.DelegatorInterface;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import static org.mockito.Mockito.when;

public class TestAverageTimeInStatusHitCollector extends TestCase {

    private static final Logger logger = Logger.getLogger(TestAverageTimeInStatusHitCollector.class);

    private static final Double VALUE_TO_RETURN_FOR_TIME_IN_SECONDS = new Double(2);

    private static final Double VALUE_TO_RETURN_FOR_NUMBER_OF_TIMES_IN_STATUS = new Double(1);

    @Mock private ConstantsManager constantsManager;
    @Mock private Status status;
    private TimeZone timeZone = TimeZone.getDefault();
    
    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);
    }

    protected DocumentHitCollector getDocumentHitCollector(
            String resolutionDateConstant,
            String averageTimeInStatusConstant,
            String status,
            Map<RegularTimePeriod, Double> totalTime,
            Map<RegularTimePeriod, Double> totalCount,
            IndexSearcher searcher,
            Class<Day> timePeriodClass,
            int days,
            boolean numberTimesInStatus) {
        return new AverageTimeInStatusHitCollector(
                resolutionDateConstant,
                averageTimeInStatusConstant,
                status,
                totalTime,
                totalCount,
                searcher,
                timePeriodClass,
                days,
                numberTimesInStatus, timeZone) {

            protected Double getSecondsInStatus(String amountOfTimeString) {
                return VALUE_TO_RETURN_FOR_TIME_IN_SECONDS;
            }

            protected Double getNumberOfTimesInStatus(String amountOfTimeString) {
                return VALUE_TO_RETURN_FOR_NUMBER_OF_TIMES_IN_STATUS;
            }
        };
    }

    public void testCollectTimeInStatus() throws Exception {
        final String averageTimeInStatusConstant = "averageTimeInStatusConstant";
        final String resolutionDateConstant = "resolutionDateConstant";
        final String customFieldValue = "foo";
        final Map<RegularTimePeriod, Double> totalTime = new LinkedHashMap<RegularTimePeriod, Double>();
        final Map<RegularTimePeriod, Double> totalCount = new LinkedHashMap<RegularTimePeriod, Double>();

        Calendar now = Calendar.getInstance();

        final List<Document> docs = new ArrayList<Document>();
        Document doc;
        now.add(Calendar.DAY_OF_YEAR, -1);
        doc = new Document();
        doc.add(new Field(averageTimeInStatusConstant, customFieldValue, Field.Store.YES, Field.Index.NOT_ANALYZED));
        doc.add(new Field(resolutionDateConstant, LuceneUtils.dateToString(now.getTime()), Field.Store.YES, Field.Index.NOT_ANALYZED));
        docs.add(doc);
        
        now.add(Calendar.DAY_OF_YEAR, -1);
        doc = new Document();
        doc.add(new Field(averageTimeInStatusConstant, customFieldValue, Field.Store.YES, Field.Index.NOT_ANALYZED));
        doc.add(new Field(resolutionDateConstant, LuceneUtils.dateToString(now.getTime()), Field.Store.YES, Field.Index.NOT_ANALYZED));
        docs.add(doc);

        DocumentHitCollector docHitCollector =  getDocumentHitCollector(
                resolutionDateConstant,
                averageTimeInStatusConstant,
                String.valueOf("Open".hashCode()),
                totalTime,
                totalCount,
                null,
                Day.class,
                3,
                true);

        when(status.getName()).thenReturn("Open");
        when(status.getId()).thenReturn(String.valueOf("Open".hashCode()));
        when(constantsManager.getStatusObjects()).thenReturn(EasyList.build(status));
        TimeInStatusDAO.setConstantsManager(constantsManager);

        for (final Iterator<Document> i = docs.iterator(); i.hasNext();) {
            docHitCollector.collect((Document) i.next());
        }

        assertEquals(4, totalCount.size());
        assertEquals(new Double(1),
                totalCount.get(
                        RegularTimePeriod.createInstance(
                                Day.class,
                                LuceneUtils.stringToDate(
                                        ((Document) docs.get(0)).get(resolutionDateConstant)
                                ),
                                TimeZone.getDefault()
                        )
                )
        );
        assertEquals(new Double(1),
                totalCount.get(
                        RegularTimePeriod.createInstance(
                                Day.class,
                                LuceneUtils.stringToDate(
                                        ((Document) docs.get(1)).get(resolutionDateConstant)
                                ),
                                TimeZone.getDefault()
                        )
                )
        );

        totalTime.clear();
        totalCount.clear();

        docHitCollector =  getDocumentHitCollector(
                resolutionDateConstant,
                averageTimeInStatusConstant,
                String.valueOf("Open".hashCode()),
                totalTime,
                totalCount,
                null,
                Day.class,
                3,
                false); /* For counting time instead of freq */

        for (final Iterator<Document> i = docs.iterator(); i.hasNext();) {
            docHitCollector.collect((Document) i.next());
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Dumping output of totalTime map");

            for (final Iterator<?> i  = totalTime.entrySet().iterator(); i.hasNext();) {
                final Map.Entry e = (Map.Entry) i.next();

                logger.debug("Key: " + e.getKey() + ";Value: " + e.getValue());
            }
        }

        assertEquals(4, totalTime.size());
        assertEquals(new Double(2),
                totalTime.get(
                        RegularTimePeriod.createInstance(
                                Day.class,
                                LuceneUtils.stringToDate(
                                        ((Document) docs.get(0)).get(resolutionDateConstant)
                                ),
                                TimeZone.getDefault()
                        )
                )
        );
        assertEquals(new Double(2),
                totalTime.get(
                        RegularTimePeriod.createInstance(
                                Day.class,
                                LuceneUtils.stringToDate(
                                        ((Document) docs.get(1)).get(resolutionDateConstant)
                                ),
                                TimeZone.getDefault()
                        )
                )
        );
    }

    protected static class TimeInStatusDAO extends com.atlassian.jira.ext.charting.field.TimeInStatusDAO {

        public TimeInStatusDAO(final DelegatorInterface delegatorInterface) {
            super(delegatorInterface);
        }

        public static void setConstantsManager(final ConstantsManager _constantsManager) {
            TimeInStatusDAO.constantsManager = _constantsManager;
        }
    }
}
