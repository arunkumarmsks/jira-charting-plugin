package it.com.atlassian.jira.ext.charting;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.admin.TimeTracking;
import com.atlassian.jira.functest.framework.assertions.TableAssertions;
import com.atlassian.jira.functest.framework.navigation.IssueNavigation;
import com.atlassian.jira.webtests.Groups;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.ADMIN_USERNAME;

/**
 * Created by IntelliJ IDEA.
 * User: ukuhnhardt
 * Date: Jun 5, 2009
 * Time: 1:57:03 PM
 * Comment: Integration Test Case for workload pie chart
 */
@Restore("testdata-export.xml")
@LoginAs(user = ADMIN_USERNAME)
public class TestWorkloadChartReport extends AbstractChartTestCase
{
    @Inject
    protected Administration administration;

    @Inject
    protected IssueNavigation issueNavigation;

    @Inject
    protected TimeTracking timeTracking;

    @Inject
    protected TableAssertions tableAssertions;

    protected void configureReport(final String projectOrFilter, final String statisticsType, final String issueTimeType)
    {
        selectProjectOrFilterId(projectOrFilter);
        tester.selectOption("statistictype", statisticsType);
        tester.selectOption("issuetimetype", issueTimeType);
        tester.submit("Next");
    }

	@Test
    public void testGenerateWorkloadPieChartGroupedByAssigneeAndTimeSpent()
    {
        administration.usersAndGroups().addUser("john.doe");
        administration.usersAndGroups().addUserToGroup("john.doe", Groups.DEVELOPERS);
        timeTracking.enable(TimeTracking.Mode.MODERN);
        String issueKey;

        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field"); // Force assignee field to be rendered in the old way
            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "First Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.setEstimates(issueKey, "1w", StringUtils.EMPTY);
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, ADMIN_USERNAME);
            issueNavigation.logWork(issueKey, "1d");

            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Second Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.setEstimates(issueKey, "2w", StringUtils.EMPTY);
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, "john.doe");
            issueNavigation.logWork(issueKey, "2d");

            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Third Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.setEstimates(issueKey, "1w", StringUtils.EMPTY);
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, "john.doe");
            issueNavigation.logWork(issueKey, "1d");

            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Forth Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.setEstimates(issueKey, "1w", StringUtils.EMPTY);
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, "john.doe");
            issueNavigation.logWork(issueKey, "1d");
        }
        finally
        {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field"); // Tell JIRA to render the new assignee field back
        }

        navigation.browseProject(TEST_PROJECT_KEY);
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Workload Pie Chart Report");
        configureReport(TEST_PROJECT_PROJECT_FILTER_DESIGNATION, "Assignee", "Time Spent");

        tester.assertTextPresent("Workload Pie Chart Report");

        assertChartImagePresent();
        tester.assertTextPresent("Data Table");
        tester.assertTableEquals(
                "singlefieldpie-report-datatable",
                new String[][]{
                        new String[]{StringUtils.EMPTY, "hours", "%"},
                        new String[]{"john.doe", "96", "80%"},
                        new String[]{"admin", "24", "20%"}
                });
    }

	@Test
    public void testGenerateWorkloadPieChartGroupedByAssigneeAndCurrentEstimate()
    {
        administration.usersAndGroups().addUser("john.doe");
        administration.usersAndGroups().addUserToGroup("john.doe", Groups.DEVELOPERS);
        timeTracking.enable(TimeTracking.Mode.MODERN);
        String issueKey;

        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field"); // Force assignee field to be rendered in the old way
            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "First Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.setEstimates(issueKey, "1w", StringUtils.EMPTY);
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, ADMIN_USERNAME);
            issueNavigation.logWork(issueKey, "1d");

            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Second Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.setEstimates(issueKey, "2w", StringUtils.EMPTY);
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, "john.doe");
            issueNavigation.logWork(issueKey, "2d");

            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Third Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.setEstimates(issueKey, "1w", StringUtils.EMPTY);
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, "john.doe");
            issueNavigation.logWork(issueKey, "1d");

            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Forth Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.setEstimates(issueKey, "1w", StringUtils.EMPTY);
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, "john.doe");
            issueNavigation.logWork(issueKey, "1d");
        }
        finally
        {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field"); // Tell JIRA to render the new assignee field back
        }

        navigation.browseProject(TEST_PROJECT_KEY);
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Workload Pie Chart Report");
        configureReport(TEST_PROJECT_PROJECT_FILTER_DESIGNATION, "Assignee", "Current Estimate");

        tester.assertTextPresent("Workload Pie Chart Report");

        assertChartImagePresent();
        tester.assertTextPresent("Data Table");
        tester.assertTableEquals(
                "singlefieldpie-report-datatable",
                new String[][]{
                        new String[]{StringUtils.EMPTY, "hours", "%"},
                        new String[]{"john.doe", "576", "80%"},
                        new String[]{"admin", "144", "20%"}

                });
    }

	@Test
	public void testGenerateWorkloadPieChartGroupedByAssigneeAndOriginalEstimate()
    {
        administration.usersAndGroups().addUser("john.doe");
        administration.usersAndGroups().addUserToGroup("john.doe", Groups.DEVELOPERS);
        timeTracking.enable(TimeTracking.Mode.MODERN);
        String issueKey;

        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field"); // Force assignee field to be rendered in the old way
            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "First Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.setEstimates(issueKey, "1w", StringUtils.EMPTY);
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, ADMIN_USERNAME);
            issueNavigation.logWork(issueKey, "1d");

            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Second Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.setEstimates(issueKey, "2w", StringUtils.EMPTY);
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, "john.doe");
            issueNavigation.logWork(issueKey, "2d");

            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Third Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.setEstimates(issueKey, "1w", StringUtils.EMPTY);
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, "john.doe");
            issueNavigation.logWork(issueKey, "1d");

            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Forth Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.setEstimates(issueKey, "1w", StringUtils.EMPTY);
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, "john.doe");
            issueNavigation.logWork(issueKey, "1d");
        }
        finally
        {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field"); // Tell JIRA to render the new assignee field back
        }

        navigation.browseProject(TEST_PROJECT_KEY);
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Workload Pie Chart Report");
        configureReport(TEST_PROJECT_PROJECT_FILTER_DESIGNATION, "Assignee", "Original Estimate");

        tester.assertTextPresent("Workload Pie Chart Report");

        assertChartImagePresent();
        tester.assertTextPresent("Data Table");
        tester.assertTableEquals(
                "singlefieldpie-report-datatable",
                new String[][]{
                        new String[]{StringUtils.EMPTY, "hours", "%"},
                        new String[]{"john.doe", "672", "80%"},
                        new String[]{"admin", "168", "20%"}

                });
    }

	@Test
    public void testGenerateWorkloadPieChartGroupedByAllFixForVersions()
    {
        administration.usersAndGroups().addUser("john.doe");
        administration.usersAndGroups().addUserToGroup("john.doe", Groups.DEVELOPERS);
        timeTracking.enable(TimeTracking.Mode.MODERN);

        administration.project().addVersion(TEST_PROJECT_KEY, "1.0", null, null);
        administration.project().addVersion(TEST_PROJECT_KEY, "2.0", null, null);
        String issueKey;

        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field"); // Force assignee field to be rendered in the old way
            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "First Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.setFixVersions(issueKey, "1.0");
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, "john.doe");
            issueNavigation.logWork(issueKey, "5h");

            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_NEWFEATURE, "Second Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.setFixVersions(issueKey, "2.0");
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, "john.doe");
            issueNavigation.logWork(issueKey, "15h");
        }
        finally
        {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field"); // Tell JIRA to render the new assignee field back
        }

        administration.project().archiveVersion(TEST_PROJECT_KEY, "1.0");

        navigation.browseProject(TEST_PROJECT_KEY);
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Workload Pie Chart Report");
        configureReport(TEST_PROJECT_PROJECT_FILTER_DESIGNATION, "Fix For Versions (all)", "Time Spent");

        tester.assertTextPresent("Workload Pie Chart Report");
        assertChartImagePresent();
        tester.assertTextPresent("Data Table");
        tester.assertTableEquals(
                "singlefieldpie-report-datatable",
                new String[][]{
                        new String[]{StringUtils.EMPTY, "hours", "%"},
                        new String[]{"2.0", "15", "75%"},
                        new String[]{"1.0", "5", "25%"}
                });
    }

	@Test
    public void testGenerateWorkloadPieChartGroupedByNonArchivedFixForVersions()
    {
        administration.usersAndGroups().addUser("john.doe");
        administration.usersAndGroups().addUserToGroup("john.doe", Groups.DEVELOPERS);
        timeTracking.enable(TimeTracking.Mode.MODERN);

        administration.project().addVersion(TEST_PROJECT_KEY, "1.0", null, null);
        administration.project().addVersion(TEST_PROJECT_KEY, "2.0", null, null);
        administration.project().addVersion(TEST_PROJECT_KEY, "3.0", null, null);
        String issueKey;

        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field"); // Force assignee field to be rendered in the old way
            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "First Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.setFixVersions(issueKey, "2.0");
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, "john.doe");
            issueNavigation.logWork(issueKey, "5h");

            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_NEWFEATURE, "Second Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.setFixVersions(issueKey, "3.0");
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, "john.doe");
            issueNavigation.logWork(issueKey, "15h");

            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Bug in Version 1.0");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.setFixVersions(issueKey, "1.0");
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, ADMIN_USERNAME);
            issueNavigation.logWork(issueKey, "500h");    // make sure it is at the top - but should not be reported as v1.0 is archived
        }
        finally
        {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field"); // Tell JIRA to render the new assignee field back
        }

        administration.project().archiveVersion(TEST_PROJECT_KEY, "1.0");

        navigation.browseProject(TEST_PROJECT_KEY);
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Workload Pie Chart Report");
        configureReport(TEST_PROJECT_PROJECT_FILTER_DESIGNATION, "Fix For Versions (non-archived)", "Time Spent");

        tester.assertTextPresent("Workload Pie Chart Report");
        assertChartImagePresent();
        tester.assertTextPresent("Data Table");
        tester.assertTableEquals(
                "singlefieldpie-report-datatable",
                new String[][]{
                        new String[]{StringUtils.EMPTY, "hours", "%"},
                        new String[]{"3.0", "15", "75%"},
                        new String[]{"2.0", "5", "25%"}
                });
    }

	@Test
    public void testGenerateWorkloadPieChartGroupedByComponent()
    {
        administration.usersAndGroups().addUser("john.doe");
        administration.usersAndGroups().addUserToGroup("john.doe", Groups.DEVELOPERS);
        timeTracking.enable(TimeTracking.Mode.MODERN);

        administration.project().addComponent(TEST_PROJECT_KEY, "Component 1", StringUtils.EMPTY, ADMIN_USERNAME);
        administration.project().addComponent(TEST_PROJECT_KEY, "Component 2", StringUtils.EMPTY, ADMIN_USERNAME);
        String issueKey;

        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field"); // Force assignee field to be rendered in the old way
            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "First Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.setComponents(issueKey, "Component 1");
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, "john.doe");
            issueNavigation.logWork(issueKey, "5h");

            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Second Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.setComponents(issueKey, "Component 2");
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, "john.doe");
            issueNavigation.logWork(issueKey, "15h");
        }
        finally
        {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field"); // Tell JIRA to render the new assignee field back
        }

        navigation.browseProject(TEST_PROJECT_KEY);
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Workload Pie Chart Report");
        configureReport(TEST_PROJECT_PROJECT_FILTER_DESIGNATION, "Components", "Time Spent");

        tester.assertTextPresent("Workload Pie Chart Report");
        assertChartImagePresent();
        tester.assertTextPresent("Data Table");
        tester.assertTableEquals(
                "singlefieldpie-report-datatable",
                new String[][]{
                        new String[]{StringUtils.EMPTY, "hours", "%"},
                        new String[]{"Component 2", "15", "75%"},
                        new String[]{"Component 1", "5", "25%"}
                });
    }

	@Test
    public void testGenerateWorkloadPieChartNoTimeTracking()
    {
        administration.usersAndGroups().addUser("john.doe");
        administration.usersAndGroups().addUserToGroup("john.doe", Groups.DEVELOPERS);

        administration.project().addComponent(TEST_PROJECT_KEY, "Component 1", StringUtils.EMPTY, ADMIN_USERNAME);
        administration.project().addComponent(TEST_PROJECT_KEY, "Component 2", StringUtils.EMPTY, ADMIN_USERNAME);
        String issueKey;

        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field"); // Force assignee field to be rendered in the old way
            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "First Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.setComponents(issueKey, "Component 1");
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, "john.doe");

            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Second Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.setComponents(issueKey, "Component 2");
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, "john.doe");
        }
        finally
        {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field"); // Tell JIRA to render the new assignee field back
        }

        navigation.browseProject(TEST_PROJECT_KEY);
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Workload Pie Chart Report");
        configureReport(TEST_PROJECT_PROJECT_FILTER_DESIGNATION, "Components", "Time Spent");

        tester.assertTextPresent("Workload Pie Chart Report");
        tester.assertTextPresent("Please enable Jira time tracking");

    }

	@Test
    public void testGenerateWorkloadPieChartNoWorklog()
    {
        administration.usersAndGroups().addUser("john.doe");
        administration.usersAndGroups().addUserToGroup("john.doe", Groups.DEVELOPERS);
        timeTracking.enable(TimeTracking.Mode.MODERN);
        String issueKey;

        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field"); // Force assignee field to be rendered in the old way
            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "First Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, "john.doe");

            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "Second Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, "john.doe");
        }
        finally
        {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field"); // Tell JIRA to render the new assignee field back
        }

        navigation.browseProject(TEST_PROJECT_KEY);
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Workload Pie Chart Report");
        configureReport(TEST_PROJECT_PROJECT_FILTER_DESIGNATION, "Assignee", "Time Spent");

        tester.assertTextPresent("Workload Pie Chart Report");
        tester.assertTextPresent("no work logged");

    }

	@Test
    public void testGenerateWorkloadPieChartGroupedByIssueType()
    {
        administration.usersAndGroups().addUser("john.doe");
        administration.usersAndGroups().addUserToGroup("john.doe", Groups.DEVELOPERS);
        timeTracking.enable(TimeTracking.Mode.MODERN);
        String issueKey;

        try {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field"); // Force assignee field to be rendered in the old way
            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "First Bug");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, "john.doe");
            issueNavigation.logWork(issueKey, "5h");

            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_NEWFEATURE, "Feature X");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, "john.doe");
            issueNavigation.logWork(issueKey, "15h");

            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_IMPROVEMENT, "Feature Y");
            issueNavigation.setPriority(issueKey, "Major");
            issueNavigation.assignIssue(issueKey, StringUtils.EMPTY, ADMIN_USERNAME);
        }
        finally
        {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field"); // Tell JIRA to render the new assignee field back
        }

        navigation.browseProject(TEST_PROJECT_KEY);
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Workload Pie Chart Report");
        configureReport(TEST_PROJECT_PROJECT_FILTER_DESIGNATION, "Issue Type", "Time Spent");

        tester.assertTextPresent("Workload Pie Chart Report");
        assertChartImagePresent();
        tester.assertTextPresent("Data Table");
        tester.assertTableEquals(
                "singlefieldpie-report-datatable",
                new String[][]{
                        new String[]{StringUtils.EMPTY, "hours", "%"},
                        new String[]{"New Feature", "15", "75%"},
                        new String[]{"Bug", "5", "25%"},
                        new String[]{"Improvement", "0", "0%"}
                });
    }

	@Test
    public void testGenerateWorkloadPieChartWithCustomFieldShowingProperLabel()
    {
        String customFieldLabel = "Labelss";

        String customFieldId = administration.customFields().addCustomField(
                "com.atlassian.jira.plugin.system.customfieldtypes:labels", customFieldLabel);

        timeTracking.enable(TimeTracking.Mode.MODERN);
        String issueKey;

        try
        {
            backdoor.darkFeatures().enableForSite("no.frother.assignee.field"); // Force assignee field to be rendered in the old way
            issueKey = issueNavigation.createIssue(TEST_PROJECT_NAME, ISSUE_TYPE_BUG, "First Bug");
            issueNavigation.setPriority(issueKey, "Major");
        }
        finally
        {
            backdoor.darkFeatures().disableForSite("no.frother.assignee.field"); // Tell JIRA to render the new assignee field back
        }

        navigation.browseProject(TEST_PROJECT_KEY);
        tester.clickLinkWithText("Reports");
        tester.clickLinkWithText("Workload Pie Chart Report");
        configureReport(TEST_PROJECT_PROJECT_FILTER_DESIGNATION, customFieldLabel, "Time Spent");

        tester.assertTextPresent("by " + customFieldLabel);
        tester.assertTextNotPresent("by " + customFieldId);
    }
}
